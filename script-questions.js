const fs = require('fs');
const firebase = require('firebase-admin');

const serviceAccount = require('./wwtbam-81fb1-firebase-adminsdk-yvzyn-41b10be187.json');
const questionsJson =  JSON.parse(fs.readFileSync('./wwtbam_json_file.json', 'utf8'));

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: "https://wwtbam-81fb1.firebaseio.com"
});

const db = firebase.firestore();
db.settings({ timestampsInSnapshots: true});

function handleQuestion (questionJson) {
  const difficulty = Math.floor(Math.random() * (13 - 1) + 1);
  const question = {
    difficulty: difficulty,
    statement: questionJson.question,
    answers: [
      { statement: questionJson.A, correct: questionJson.answer === 'A' },
      { statement: questionJson.B, correct: questionJson.answer === 'B' },
      { statement: questionJson.C, correct: questionJson.answer === 'C' },
      { statement: questionJson.D, correct: questionJson.answer === 'D' },
    ]
  };

  db.collection('questions').add(question).then(ref => {
    // console.log(`[${ref.data().difficulty}] ${ref.data().statement}, A: ${ref.data().answers[0].statement} (${ref.data().answers[0].correct}), B: ${ref.data().answers[1].statement} (${ref.data().answers[1].correct}), C: ${ref.data().answers[2].statement} (${ref.data().answers[2].correct}), D: ${ref.data().answers[3].statement} (${ref.data().answers[3].correct})`);
  });
}

questionsJson.forEach(handleQuestion);
console.log(`${questionsJson.length} questions imported`);